package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Admin;
import model.Company;
import util.DbUtil;
import util.QueryUtil;

public class IAdminDaoImpl implements IAdminDao {

	PreparedStatement pst;
	ResultSet rs;
	int result;

	@Override
	public int adminLogin(Admin admin) {
		result = 0;
		try {
			pst = DbUtil.getCon().prepareStatement(QueryUtil.adminLogin);
			pst.setString(1, admin.getUid());
			pst.setString(2, admin.getPassword());
			rs = pst.executeQuery();
			while (rs.next()) {
				result++;
			}
		} catch (ClassNotFoundException | SQLException e) {

			System.out.println("Exception occures in adminAuthentication");
		}
		return result;
	}

	@Override
	public List<Company> viewAll() {
		List<Company> list = new ArrayList<Company>();
		try {
			pst = DbUtil.getCon().prepareStatement(QueryUtil.viewALL);
			rs = pst.executeQuery();
			while (rs.next()) {
				Company c = new Company(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4),
						rs.getInt(5),rs.getInt(6));
				list.add(c);
			}
		} catch (ClassNotFoundException | SQLException e) {

			System.out.println("Exception occures in View All");
		}
		return list;
	}

	@Override
	public void delete(Company c) {
		try {
			pst = DbUtil.getCon().prepareStatement(QueryUtil.delete);
			pst.setInt(1, c.getCid());
			pst.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {

		}

	}

	@Override
	public void update(Company c) {
		try {
			pst = DbUtil.getCon().prepareStatement(QueryUtil.update);
			pst.setString(1, c.getCname());
			pst.setInt(2, c.getCid());
			pst.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {

		}

	}

	@Override
	public int add(Company c) {
		result = 0;
		try {
			pst = DbUtil.getCon().prepareStatement(QueryUtil.add);
			pst.setInt(1,c.getCid());
			pst.setString(2, c.getCname());
			pst.setString(3, c.getCemail());
			pst.setInt(4, c.getPer10());
			pst.setInt(5, c.getPer12());
			pst.setInt(6, c.getDeg());
			result = pst.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			return result;
		}
		return result;
	}

}
