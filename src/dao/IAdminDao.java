package dao;

import java.util.List;

import model.Admin;
import model.Company;

public interface IAdminDao {

	public int adminLogin(Admin admin);

	public List<Company> viewAll();
	
	public void delete(Company c);
	
	public void update(Company c);
	
	public int add(Company c);
}
