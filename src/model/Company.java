package model;

public class Company {

	private Integer cid;
	 private String cname;
	 private String cemail;
		private Integer per10;
		private Integer per12;
		private Integer deg;
		public Company() {
			// TODO Auto-generated constructor stub
		}
		public Company(Integer cid, String cname, String cemail, Integer per10, Integer per12, Integer deg) {
			super();
			this.cid = cid;
			this.cname = cname;
			this.cemail = cemail;
			this.per10 = per10;
			this.per12 = per12;
			this.deg = deg;
		}
		public Integer getCid() {
			return cid;
		}
		public void setCid(Integer cid) {
			this.cid = cid;
		}
		public String getCname() {
			return cname;
		}
		public void setCname(String cname) {
			this.cname = cname;
		}
		public String getCemail() {
			return cemail;
		}
		public void setCemail(String cemail) {
			this.cemail = cemail;
		}
		public Integer getPer10() {
			return per10;
		}
		public void setPer10(Integer per10) {
			this.per10 = per10;
		}
		public Integer getPer12() {
			return per12;
		}
		public void setPer12(Integer per12) {
			this.per12 = per12;
		}
		public Integer getDeg() {
			return deg;
		}
		public void setDeg(Integer deg) {
			this.deg = deg;
		}
		
	 
	
}

