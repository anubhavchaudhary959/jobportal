package controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IAdminDao;
import dao.IAdminDaoImpl;
import model.Admin;
import model.Company;

@WebServlet(name = "AdminController", urlPatterns = { "/login", "/add", "/updateCont" })
public class AdminController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url = request.getServletPath();
		IAdminDao dao = new IAdminDaoImpl();

		if (url.equals("/login")) {
			String uid = request.getParameter("uid");
			String password = request.getParameter("password");
			HttpSession session = request.getSession(true);
			session.setAttribute("uid", uid);
			System.out.println(uid + " Logged in @ " + new Date(session.getCreationTime()));

			Admin admin = new Admin(uid, password);
			int result = dao.adminLogin(admin);
			if (result > 0) {
				List<Company> list = dao.viewAll();
				request.setAttribute("comp", list);
				request.getRequestDispatcher("admin.jsp").forward(request, response);
			} else {
				request.setAttribute("error", "Hi " + uid + " , Please check your credentials");
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
		} else if (url.equals("/add")) {
			Integer cid = Integer.parseInt(request.getParameter("company id"));
			String cname = request.getParameter("company name");
			String cemail = request.getParameter("company email");
			Integer per10 = Integer.parseInt(request.getParameter("percenatge 10 required"));
			Integer per12 = Integer.parseInt(request.getParameter("percenatge 12 required"));
			Integer deg = Integer.parseInt(request.getParameter("percenatge degree required"));
			
			Company c = new Company(cid,cname, cemail,per10,per12,deg);
			int result = dao.add(c);
			if (result > 0) {
				request.setAttribute("msg", cid + " inserted Successfully");
			} else {
				request.setAttribute("msg", cid + " duplicate Entry");
			}
			List<Company> list = dao.viewAll();
			request.setAttribute("comp", list);
			request.getRequestDispatcher("admincrud.jsp").forward(request, response);
		} else if (url.contentEquals("/updateCont")) {
			int cid = Integer.parseInt(request.getParameter("cid"));
			String cname = request.getParameter("company name");
			Company c = new Company();
			c.setCid(cid);
			c.setCname(cname);
			dao.update(c);
			List<Company> list = dao.viewAll();
			request.setAttribute("comp", list);
			request.getRequestDispatcher("admincrud.jsp").include(request, response);
		}
	}
}
