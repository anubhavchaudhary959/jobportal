package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IAdminDao;
import dao.IAdminDaoImpl;
import model.Company;

@WebServlet(name = "AdminCrudController", urlPatterns = { "/admincrud", "/home", "/delete", "/insert", "/signout",
		"/update" })
public class AdminCrudController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String url = request.getServletPath();
		IAdminDao dao = new IAdminDaoImpl();
		Company c = new Company();

		if (url.contentEquals("/admincrud")) {

			List<Company> list = dao.viewAll();
			request.setAttribute("comp", list);
			request.getRequestDispatcher("admincrud.jsp").include(request, response);
		} else if (url.equals("/home")) {
			List<Company> list = dao.viewAll();
			request.setAttribute("comp", list);
			request.getRequestDispatcher("admin.jsp").forward(request, response);
		}

		else if (url.equals("/delete")) {

			int cid = Integer.parseInt(request.getParameter("company id"));
			c.setCid(cid);
			dao.delete(c);
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			out.print(cid + " deleted Successfully<br/>");
			List<Company> list = dao.viewAll();
			request.setAttribute("comp", list);
			request.getRequestDispatcher("admincrud.jsp").include(request, response);
			// request.getRequestDispatcher("admincrud").include(request, response);//dont
			// redirect
		}

		else if (url.equals("/insert")) {
			request.getRequestDispatcher("insert.jsp").forward(request, response);
		}

		else if (url.equals("/signout")) {
			HttpSession session = request.getSession(false);
			String uid = (String) session.getAttribute("uid");
			System.out.println(uid + " logged out @ " + new Date());
			session.invalidate();
			request.setAttribute("uid", uid + " Logged out successfully!!!");
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}

		else if (url.equals("/update")) {
			int cid = Integer.parseInt(request.getParameter("company id"));
			String cname = request.getParameter("company Name");

			c.setCid(cid);
			c.setCname(cname);
			request.setAttribute("comp", c);
			request.getRequestDispatcher("update.jsp").forward(request, response);
		}
	}
}
