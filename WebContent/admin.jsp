<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin</title>
</head>
<body>
	<h1>Admin Home Page</h1>
	<hr />
	<p align="right">
	<%
	response.setHeader("Cache-control", "no-cache");
	response.setHeader("Cache-control", "no-store");
	response.setHeader("Pragma","no-cache");
	response.setDateHeader("Expire",0);
	%>
	Hi , <%=(String) session.getAttribute("uid") %><br/>
	<a href ="admincrud" style = "text-decoration:none">Admin CRUD</a>&nbsp;&nbsp;
		<a href="signout">Logout</a>&nbsp;&nbsp;
	</p>
	<hr />
	<div align="center" >
	<table border="6">
		<tr>
			<th colspan="6">Company Details</th>
		</tr>
		<tr>
			<th>Company id</th>
			<th>Company name</th>
			<th>Company email</th>
			<th>Per10</th>
			<th>Per12</th>
	        <th>Degree</th>
	
		</tr>
		<c:forEach items="${com}" var="f">
			<tr style="text-align:center">
				<td>${e.getCid()}</td>
				<td>${e.getCname()}</td>
				<td>${e.getCemail()}</td>
				<td>${e.getPer10()}</td>
				<td>${e.getPer12()}</td>
				<td>${e.getDeg()}</td>
				
			</tr>
		</c:forEach>

	</table>
	</div>
</body>
</html>