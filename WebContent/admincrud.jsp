<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin CRUD</title>
</head>
<body>

<h1>Admin CRUD Operation Page</h1>
	
	<p align="right">
	<a href= "home">Home</a>&nbsp;&nbsp;
	<a href= "insert">Add New Company</a>&nbsp;&nbsp;
		<a href="signout">Logout</a>&nbsp;&nbsp;
	</p>
	<hr />
	${msg}
	<div align="center" >
	<table border="6">
		<tr>
			<th colspan="8">Company Details</th>
		</tr>
		<tr style="text-align: center">
			<th>Company id</th>
			<th>Company name</th>
			<th>Company email</th>
			<th>Per10</th>
			<th>Per12</th>
	        <th>Degree</th>
	
			<th>Update</th>
			<th>Delete</th>
		</tr>
		<c:forEach items="${com}" var="e">
			<tr style="text-align:center">
				<td>${e.getCid()}</td>
				<td>${e.getCname()}</td>
				<td>${e.getCemail()}</td>
				<td>${e.getPer10()}</td>
				<td>${e.getPer12()}</td>
				<td>${e.getDeg()}</td>
				
				<td><a style="text-decoration: none"
					href="update?cid=${e.getCid()}&Cname=${e.getCname()}">Update</a></td>
				<td><a style="text-decoration: none"
					href="delete?cid=${e.getCid()}">Delete</a></td>
			</tr>
		</c:forEach>

	</table>
	</div>
	

</body>
</html>